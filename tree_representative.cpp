#include "dynamic_node.h"


TreeRepresentative::TreeRepresentative(unsigned long rank) : BiasedBinaryNode<DynamicNode*>(rank) {}

std::shared_ptr<BiasedBinaryNode<DynamicNode*>> TreeRepresentative::tilt_right(){
  auto other = this->left_child();
  if(other == nullptr)  return this->me();
  this->value()->decode_rel_path_and_struct_w();
  other->value()->decode_rel_path_and_struct_w();

  if(other->left_child() != nullptr) other->left_child()->value()->decode_rel_struct_w();
  if(other->right_child() != nullptr) other->right_child()->value()->decode_rel_struct_w();
  if(this->right_child() != nullptr) this->right_child()->value()->decode_rel_struct_w();

  auto new_root = BiasedBinaryNode<DynamicNode*>::tilt_right();

  if(this->left_child() == other){
    other->value()->encode_rel_path_and_struct_w();
    this->value()->encode_rel_path_and_struct_w();
  }else{
    this->value()->encode_rel_path_and_struct_w();
    other->value()->encode_rel_path_and_struct_w();
  }
  return new_root;
}

std::shared_ptr<BiasedBinaryNode<DynamicNode*>> TreeRepresentative::tilt_left(){
  auto other = this->right_child();
  if(other == nullptr)  return this->me();
  this->value()->decode_rel_path_and_struct_w();
  other->value()->decode_rel_path_and_struct_w();

  if(other->left_child() != nullptr) other->left_child()->value()->decode_rel_struct_w();
  if(other->right_child() != nullptr) other->right_child()->value()->decode_rel_struct_w();
  if(this->left_child() != nullptr) this->left_child()->value()->decode_rel_struct_w();

  auto new_root = BiasedBinaryNode<DynamicNode*>::tilt_right();

  if(this->right_child() == other){
    other->value()->encode_rel_path_and_struct_w();
    this->value()->encode_rel_path_and_struct_w();
  }else{
    this->value()->encode_rel_path_and_struct_w();
    other->value()->encode_rel_path_and_struct_w();
  }
  return new_root;
}

void TreeRepresentative::rewire_right_child_with(std::shared_ptr<BiasedBinaryNode<DynamicNode*>> new_child){

}
