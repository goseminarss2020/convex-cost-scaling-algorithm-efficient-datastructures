#include "biased_binary_tree.h"
#include <vector>
#include <iostream>

int main(){
  BiasedBinaryTree<int> start (std::make_shared<BiasedBinaryNode<int>>(5));
  for(int i = 1; i < 5; i++){
    std::cout << "joining " << i << std::endl;
    start.join(BiasedBinaryTree(std::make_shared<BiasedBinaryNode<int>>(2)));
  }
  BiasedBinaryTree<int> second (std::make_shared<BiasedBinaryNode<int>>(5));
  std::shared_ptr<BiasedBinaryNode<int>> split_pos = second.root();
  for(int i = 1; i < 5; i++){
    std::cout << "joining " << i << std::endl;
    second.join(BiasedBinaryTree(std::make_shared<BiasedBinaryNode<int>>(i)));
  }
  start.join(second);
  std::cout << "----------------" << std::endl;
  std::cout << start;
  BiasedBinaryTree<int> before;
  BiasedBinaryTree<int> after;
  std::cout << "-.-.-.-" << std::endl;
  std::cout << "use_count : " << split_pos.use_count() << std::endl;
  std::shared_ptr<BiasedBinaryNode<int>> removed_parent, removed_root;
  start.split_at(split_pos, before, after, removed_parent, removed_root);
  std::cout << "----------------" << std::endl;
  std::cout << before;
  std::cout << "----------------" << std::endl;
  std::cout << after;
  std::cout << split_pos;
}
