#ifndef BIASED_BINARY_TREE_H
#define BIASED_BINARY_TREE_H

#include "biased_binary_node.h"

template <typename T>
class BiasedBinaryTree;

template <typename T>
std::ostream& operator<<(std::ostream&, BiasedBinaryTree<T>&);

template <typename T>
class BiasedBinaryTree{
  std::shared_ptr<BiasedBinaryNode<T>> _root;
public:
  BiasedBinaryTree(){}
  BiasedBinaryTree(std::shared_ptr<BiasedBinaryNode<T>>);
  friend std::ostream& operator<<<>(std::ostream&, BiasedBinaryTree<T>&);

  std::shared_ptr<BiasedBinaryNode<T>>& root();
  void join(BiasedBinaryTree<T>);
  void split_at(std::shared_ptr<BiasedBinaryNode<T>>, BiasedBinaryTree<T>&, BiasedBinaryTree<T>&, std::shared_ptr<BiasedBinaryNode<T>>, std::shared_ptr<BiasedBinaryNode<T>>);
};

#include "biased_binary_tree.cpp"

#endif
