CXXFLAGS = -O0 -g -std=c++2a -Wall -Wextra -Wpedantic

dynamic_test: dynamic_test.o tree_representative.o dynamic_node.o
	$(CXX) $(CXXFLAGS) -o $@ $^

dynamic_test.o: dynamic_test.cpp dynamic_node.o
	$(CXX) $(CXXFLAGS) -c $<

tree_representative.o: tree_representative.cpp dynamic_node.h
	$(CXX) $(CXXFLAGS) -c $<

dynamic_node.o: dynamic_node.cpp dynamic_node.h biased_binary_node.o
	$(CXX) $(CXXFLAGS) -c $<

biased_test: tree_test.o
	$(CXX) $(CXXFLAGS) -o $@ $^

tree_test.o: tree_test.cpp biased_binary_tree.o
	$(CXX) $(CXXFLAGS) -c $<

biased_binary_tree.o: biased_binary_tree.cpp biased_binary_tree.h biased_binary_node.o
	$(CXX) $(CXXFLAGS) -c $<

biased_binary_node.o: biased_binary_node.cpp biased_binary_node.h
	$(CXX) $(CXXFLAGS) -c $<

clean:
	rm -f *.o *.gch biased_test dynamic_test

.PHONY: clean
