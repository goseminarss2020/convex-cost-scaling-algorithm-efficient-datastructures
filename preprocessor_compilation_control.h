#ifndef PREPROCESSOR_COMPILATION_CONTROL_H
#define PREPROCESSOR_COMPILATION_CONTROL_H

/*
  Define DEBUG for evaluation of assertions
*/
#define DEBUG


/*
  Define for Protocol of join and split cases
*/
#define PROTOCOL

#endif
