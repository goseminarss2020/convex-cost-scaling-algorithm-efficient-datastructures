#ifndef BIASED_BINARY_TREE_CPP
#define BIASED_BINARY_TREE_CPP

#include "biased_binary_tree.h"

template <typename T>
BiasedBinaryTree<T>::BiasedBinaryTree(std::shared_ptr<BiasedBinaryNode<T>> root) : _root(root){}

template <typename T>
std::shared_ptr<BiasedBinaryNode<T>>& BiasedBinaryTree<T>::root(){
  return this->_root;
}

template <typename T>
void BiasedBinaryTree<T>::join(BiasedBinaryTree<T> other){
  if(this->root() != nullptr){
    this->root() = this->root()->global_join(other.root());
  }else{
    this->root() = other.root();
  }
  other.root() = std::shared_ptr<BiasedBinaryNode<T>>();
}

template <typename T>
void BiasedBinaryTree<T>::split_at(std::shared_ptr<BiasedBinaryNode<T>> split_pos,BiasedBinaryTree<T>& before, BiasedBinaryTree<T>& after, std::shared_ptr<BiasedBinaryNode<T>> removed_interior_parent, std::shared_ptr<BiasedBinaryNode<T>> removed_interior_root){
  std::shared_ptr<BiasedBinaryNode<T>> root_before;
  std::shared_ptr<BiasedBinaryNode<T>> root_after;
  this->root()->split_at(split_pos, root_before, root_after, removed_interior_parent, removed_interior_root);
  before = BiasedBinaryTree<T>(root_before);
  after = BiasedBinaryTree<T>(root_after);
}

template <typename T>
std::ostream& operator<<(std::ostream& os, BiasedBinaryTree<T>& tree){
  if(tree.root() != nullptr) os << *tree.root();
  return os;
}

#endif
