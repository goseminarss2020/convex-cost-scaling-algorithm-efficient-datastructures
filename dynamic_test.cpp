#include "dynamic_node.h"
#include <iostream>

int main(){
  DynamicNode node (5);
  std::cout << node.global_reversed() << std::endl;
  node.reverse();
  std::cout << node.global_reversed() << std::endl;
  return 0;
}
