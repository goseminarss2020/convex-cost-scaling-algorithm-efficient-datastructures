Purpose
===
This was supposed to be a implementation of the _"Dynamic Tree"_ data structure described in **[1]**. Specifically using the recommended _"Biased (Binary) Tree"_ data structure described in **[2]**. Sadly this could not be accomplished in time for submission.

Supported Operations
===
At time of submission the _BiasedBinaryTree<T>_ supports splitting and joining, which are the key operations needed for an efficient implementation of the Algorithm described in **[3]**. A test of these can be compiled using **make biased_test**.

_TreeRepresentative_ :_BiasedBinaryTree\<DynamicAddon\>_ is the type maintaining the binary tree structure described in **[1]** and is supposed to maintain _DynamicAddon_'s invariants. It is currently mostly a skeleton.

_DynamicAddon_ is the Type providing most of the functionality. Its implementation, too, is incomplete.

Plans
===
If changes will be made they will go to a different branch, meaning the current state will be archived for evaluation. It seems unlikely that any pushes will be made during the exam period.

---
* **[1]** Daniel D. Sleator, Robert Endre Tarjan _A Data Structure for Dynamic Trees_ Journal of Computer and System Science (1983)
* **[2]** Samuel W. Bent, Daniel D. Sleator, Robert E. Tarjan _Biased Search Trees_ Society for Industrial and Applied Mathematics (1985)
* **[3]** Ravindra K. Ahuja, Dorit S. Hochbaum, James B. Orlin _Solving the Convex Cost Integer Dual Network Flow Problem_ Management Science (2003)
